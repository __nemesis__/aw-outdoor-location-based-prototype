namespace Mapbox.Examples
{
    using UnityEngine;
    using Mapbox.Utils;
    using Mapbox.Unity.Map;
    using Mapbox.Unity.MeshGeneration.Factories;
    using Mapbox.Unity.Utilities;
    using System.Collections.Generic;
    using System;

    public class SpawnOnMap : MonoBehaviour
    {
        private static int MAX_OBJ_INST = 100;

        [SerializeField]
        AbstractMap _map;

        [SerializeField]
        [Geocode]
        string[] _locationStrings;
        Vector2d[] _locations;

        [SerializeField]
        float _spawnScale = 10f;

        [SerializeField]
        GameObject _markerPrefab;

        public GameObject _my_markerPrefab;

        private int numObj;
        private int spawnRuntime = 0; //contatore per fare lo Spawn a Runtime dopo un certo numero di iterazioni

        //private System.Random rnd = new System.Random();

        List<GameObject> _spawnedObjects;

        void Start()
        {
            _locations = new Vector2d[MAX_OBJ_INST];

            _spawnedObjects = new List<GameObject>();
            for (int i = 0; i < _locationStrings.Length; i++)
            {
                var locationString = _locationStrings[i]; //ipotetico valore: "43.90485, 12.28623"

                _locations[i] = Conversions.StringToLatLon(locationString);
                var instance = Instantiate(_markerPrefab);
                Vector3 pos = _map.GeoToWorldPosition(_locations[i], true);
                pos.y = pos.y + 1; // per posizionare l'oggetto pi� in alto
                instance.transform.localPosition = pos;
                instance.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
                _spawnedObjects.Add(instance);
            }
            numObj = _locationStrings.Length;

        }

        private void Update()
        {
            int count = _spawnedObjects.Count;
            for (int i = 0; i < count; i++)
            {
                var spawnedObject = _spawnedObjects[i];
                var location = _locations[i];
                Vector3 pos = _map.GeoToWorldPosition(location, true);
                pos.y = pos.y + 1; // per posizionare l'oggetto pi� in alto
                spawnedObject.transform.localPosition = pos;
                spawnedObject.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
            }

            //Test Per lo spawn a runtime
            spawnRuntime++;
            if (spawnRuntime == 300 /*rnd.Next(0,10) > 7*/) /*dopo "300" iterazioni istanzio un nuovo oggetto*/
            {
                instantiateRunTime();
            }
        }

        private void instantiateRunTime()
        {
            string locationString;

            locationString = "44.235492, 12.061695";
            _locations[numObj] = Conversions.StringToLatLon(locationString); // aggiungo le coordinate all'array di locations

            GameObject instance = Instantiate(_my_markerPrefab);

            Vector3 pos = _map.GeoToWorldPosition(_locations[numObj], true);
            pos.y = pos.y + 1; // per posizionare l'oggetto pi� in alto
            instance.transform.localPosition = pos;
            instance.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);

            _spawnedObjects.Add(instance);
            numObj++;
        }
    }
}
