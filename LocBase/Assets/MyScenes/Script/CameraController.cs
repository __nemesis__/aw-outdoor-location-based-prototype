﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
   
    public GameObject player;

	// Use this for initialization
	void Start () {

        Vector3 position = new Vector3(player.transform.position.x, player.transform.position.y + 1 , player.transform.position.z);
        transform.position = position;

        transform.rotation = player.transform.rotation;

	}
	
	// Update is called once per frame
	void LateUpdate () {
        /*sposto la camera nella posizione del giocatore*/
        Vector3 pos = new Vector3(player.transform.position.x, player.transform.position.y + 2, 
        player.transform.position.z);
        transform.position = pos;

        /*ruoto la camera dal punto di vista del giocatore*/
        transform.rotation = player.transform.rotation;
    }
}
